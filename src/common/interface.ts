//api数据返回
export interface requeryRes {
  code?: number; //状态码
  msg?: string; //信息/消息
  other?: string; //其他 扩展字段
  data?: any; //数据
  total?: number; //数据总数，分页用到
}

//路由
export interface routerItem {
  path: string; //路径
  name: string; //路由名称，生成menu时menu name
  //额外信息，icon为menu中的icon
  meta?: {
    icon: string;
  };
  lang?: string; //多语言配置,如果没有配置，则调用name字段显示
  children?: routerItem[]; //子路由，menu中的子menu 没有时可为空数组
  redirect?: string; //重定向页面
  component: Component;
}
