const ENV = import.meta.env; //引入环境变量
console.log("当前环境 " + ENV.VITE_BASE_API);

let domain = ENV.VITE_BASE_URL; //访问后台的域名

let baseUrl = ENV.VITE_BASE_API; //api请求的域名

let dev = ENV.VITE_MODE === "development"; //是否测试模式 true是 false否

let UEditorUrl = domain + "vue/UEditor/"; //访问 UEditor 静态资源的根路径

let serverImg = domain + "uploads/"; //服务器图片前缀

let qnUrl = "http://qiniu.other.88an.top/";

export default {
  dev,
  serverImg,
  domain,
  baseUrl,
  UEditorUrl,
  qnUrl
};
