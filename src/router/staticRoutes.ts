import type { routerItem } from "@/common/interface";
import wrapper from "@/layout/wrapper.vue";
import home from "@/views/index/home.vue";
import edit from "@/views/index/edit.vue";

const staticRoutes: routerItem[] = [
  {
    path: "/",
    name: "首页",
    lang: "menu.t1",
    component: home,
    children: [],
    meta: {
      icon: "custom-icon-Home",
    },
  },
  {
    path: "/edit",
    name: "商品管理",
    lang: "menu.t8",
    component: wrapper,
    redirect: "/goods/edit",
    meta: {
      icon: "custom-icon-Bag1",
    },
    children: [
      {
        path: "/goods/edit",
        name: "添加商品",
        lang: "menu.t5",
        children: [],
        component: edit,
      },
    ],
  }
];
export default staticRoutes;
