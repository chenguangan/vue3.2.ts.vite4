import type { RouteRecordRaw } from "vue-router";
import { createRouter, createWebHashHistory } from "vue-router";
import staticRoutes from "./staticRoutes";
import defaultRoutes from "./defaultRoutes";

const router = createRouter({
  history: createWebHashHistory(),
  routes: [...staticRoutes, ...defaultRoutes] as RouteRecordRaw[],
});

router.beforeEach((to, from, next) => {
  let token = localStorage.getItem("token");
  if (to.path === "/login") {
    next();
  } else {
    if (token) {
      next();
    } else {
      next({
        path: "/login",
      });
    }
  }
});
export default router;
