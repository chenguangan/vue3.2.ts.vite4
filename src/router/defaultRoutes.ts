import type { routerItem } from "@/common/interface";
import test from "@/views/sys/test.vue";
import notFound from "@/views/sys/notFound.vue";
const defaultRoutes: routerItem[] = [
  {
    path: "/test",
    name: "内部页面",
    component: test,
  },
  {
    path: "/login",
    name: "登录",
    component: () => import("@/views/sys/login.vue"),
  },
  {
    path: "/pass",
    name: "修改密码",
    component: () => import("@/views/sys/pass.vue"),
  },
  {
    path: "/:pathMatch(.*)*",
    name: "404",
    component: notFound,
  },
];

export default defaultRoutes;
